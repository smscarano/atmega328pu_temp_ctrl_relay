#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 13
#define ONE_WIRE_BUS2 12

const char relay = 8;
const int led = 9;
boolean relay_state = false;
float temp;
float temp2;
float max_temp = 35;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature temp_sensor(&oneWire);

OneWire oneWire2(ONE_WIRE_BUS2);
DallasTemperature temp_sensor2(&oneWire2);

void getTemps() {
  temp_sensor.requestTemperatures();
  temp =  temp_sensor.getTempCByIndex(0);
  Serial.print(temp);
  Serial.print(" - ");
  temp_sensor2.requestTemperatures();
  temp2 =  temp_sensor2.getTempCByIndex(0);
  Serial.println(temp2);
  Serial.println();
}

void handleTemp(){
  if ( relay_state == false && temp >= max_temp ) {
    relay_state = true;
    digitalWrite(relay, LOW);
    Serial.println("relay on");
  }
  else if( relay_state == true && temp < max_temp ) {
    relay_state = false;
    digitalWrite(relay, HIGH);
    Serial.println("relay off");
  }
}

void setup(void) {
  Serial.begin( 9600 );
  pinMode( relay , OUTPUT );
  pinMode( led , OUTPUT );
  digitalWrite( led , HIGH );//Initialize led on
  digitalWrite( relay , HIGH );//Initialize relay off
}

void loop(void) {
  getTemps();
  handleTemp();
  delay(5000);
}
